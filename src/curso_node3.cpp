#include "ros/ros.h"
#include "std_msgs/Int16MultiArray.h"
#include <iostream>

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void chatterCallback(const std_msgs::Int16MultiArray::ConstPtr& msg)
{
    double v = 18.0, a = 2.0, p = 14.0;

  for (int i = 0; i < msg->data.size(); i++)
  {
     std::cout << char(msg->data[i]/50);
  }
  std::cout << std::endl;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/even_more_numbers", 1000, chatterCallback);

  ros::spin();

  return 0;
}