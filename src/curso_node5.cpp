#include "ros/ros.h"
#include "std_msgs/Int32MultiArray.h"
#include <iostream>

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void chatterCallback(const std_msgs::Int32MultiArray::ConstPtr& msg)
{
  for (int i = 0; i < msg->data.size(); i++)
  {
     std::cout << char(msg->data[i] - 1993);
  }
  std::cout << std::endl;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/unlock_me_if_you_can", 1000, chatterCallback);

  ros::spin();

  return 0;
}